FROM nginx:alpine
WORKDIR /pasta-de-trabalho-projeto
ADD . .
COPY ./nginx.conf /etc/nginx/nginx.conf
RUN rm -rf /usr/share/nginx/html/* 
COPY . /usr/share/nginx/html
EXPOSE 80
ENTRYPOINT ["nginx", "-g", "daemon off;"]
